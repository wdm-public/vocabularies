# vocabularies
All vocabularies are encoded in UTF-8 (Unicode) and especially not in ISO 8859-1 (latin-1). 
Mind that Microsoft Excel interprets CSV files by default as ANSI encoded, which has an impact on the diplay of "Umlaute".
Use Notepad++ for editing instead.